import React from 'react';
import ReactDOM from 'react-dom';
import InlineEdit from 'react-edit-inline';
import WaitingPeriodPopover from './WaitingPeriodPopover.jsx';
import CafeteriaSelector from './Contribution/CafeteriaSelector.jsx';
import Button from 'react-bootstrap/lib/Button';
import BenefitSelector from './BenefitSelector.jsx'

class OfferItem extends React.Component{
  constructor(props) {
    super(props);
    this.state = { show: false };
  }

  _onUpdateOffer(offer) {
    this.setState({disableContribution: !!offer.contribution});
    this.props.onUpdateOffer(offer);
  }

  componentDidMount() {
    let $offerElement = $(this.refs.offerItem);
    $offerElement.effect('highlight');
  }

  dataChanged(data) {
    let offer = this.props.offer;
    offer.name = data.name;
    this.props.onUpdateOffer(offer);
  }

  deleteOffer(e) {
    e.preventDefault();
    let $offerElement = $(this.refs.offerItem);
    $offerElement.effect('highlight',{color: '#ff0000'}, () => { //highlight red before delete
      this.props.onRemoveOffer(this.props.offer);
    });
  }

  _onUpdateBenefit(benefitObject, planType) {
    let offer = this.props.offer;
    offer[planType] = benefitObject;
    this.props.onUpdateOffer(offer);
    this.forceUpdate();
  }

  _renderBenefitSelectors() {
    let keys = _.keys(this.props.planTypes);
    let disableContribution =  this.state.disableContribution || !!this.props.offer.contribution;
    return keys.map(function(plan) {
      let plansList = _.where(this.props.plansList, {type: plan});
      let benefitObject = this.props.offer[plan]  || {};
      return (
       <BenefitSelector key={'planSelect_'+plan} disableContribution={disableContribution}
                        benefit={benefitObject} plan={plan} onUpdateBenefit={this._onUpdateBenefit.bind(this)}
                        plansList={plansList} planTypes={this.props.planTypes} />
      );
    }, this);
  }

  _peopleFilter() {
    if (!this.props.offer.default) {
      return (
        <div className="rule-field">
            <span className="editable-click" onClick={this.props.onShowRuleBuilder.bind(this, this.props.offer)}>{this.props.offer.rule || 'Add rule'}</span>
        </div>)
    }
  }

  _displayName() {
    if (this.props.offer.default) {
      return (<div className="col-md-12"><div className="title"><h3>Base Offering</h3></div></div>);
    } else {
      return (
          <div className="col-md-12">
            <div className="title">
              <h3>Employee Class:</h3>
              <InlineEdit className="editable-click" activeClassName="name-input"
                          text={this.props.offer.name || 'Name'}
                          paramName="name" change={this.dataChanged.bind(this)}   />
            </div>
            <div className="options">
              {!this.props.firstItem ?
                <div className="menu-option">
                  <i className="sprite sprite-moveUp" />
                   <Button bsStyle="link" onClick={this.props.onSort.bind(this, this.props.offer, this.props.offer.order - 1)}>Move up</Button>
                </div>
                : null}
              {!this.props.lastItem ?
                <div className="menu-option">
                  <i className="sprite sprite-moveDown" />
                   <Button bsStyle="link" onClick={this.props.onSort.bind(this, this.props.offer, this.props.offer.order + 1)}>Move down</Button>
                </div>
                : null}
              <div className="menu-option">
                <i className="sprite sprite-trash" />
                <a className="delete-btn" href="#" onClick={this.deleteOffer.bind(this)}>Delete</a>
              </div>
            </div>
          </div>);
    }
  }

  _displaySubTitle() {
    if (this.props.offer.default) {
      return (<span>Employees that cannot be matched to a class will be offered the Base Offering</span>);
    } else {
      return (<span>Employees in this class are matching this rule:</span>);
    }
  }

  _disableCafeteria() { //If has contribution disable cafeteria
    let disableCafeteria = false;
    _.forEach(this.props.planTypes, (val) => {
        let benefit = this.props.offer[val];
          if (benefit) {
            let contribution = benefit.contribution;
            if ((!Array.isArray(contribution) && contribution) || (Array.isArray(contribution) && contribution.length > 0)) {
              disableCafeteria = true;
              return false;
            }
        }
    });

    return disableCafeteria;
  }

  render() {
    return (
     <li ref="offerItem" className="row eligibility-item">
       <div className="offer-header">
         {this._displayName()}
         <div className="col-md-12 subTitle">
           {this._displaySubTitle()}
           {this._peopleFilter()}
         </div>
       </div>
       <div className="row">
         <div className="col-md-12">
            <h3 className="offered-benefits">Offered Benefits</h3>
         </div>
       </div>
       <WaitingPeriodPopover defaultView entity={this.props.offer} onUpdate={this.props.onUpdateOffer}/>
       <CafeteriaSelector disable={this._disableCafeteria()} entity={this.props.offer} onUpdate={this._onUpdateOffer.bind(this)}/>
       <div>
         <div className="eligibility-form">
           {this._renderBenefitSelectors()}
         </div>
       </div>
     </li>
    );
  }
}

OfferItem.propTypes = {
  plansList: React.PropTypes.array,
  planTypes: React.PropTypes.object,
  offer: React.PropTypes.object,
  onUpdateOffer: React.PropTypes.func,
  onRemoveOffer: React.PropTypes.func,
  onSort: React.PropTypes.func,
  onShowRuleBuilder: React.PropTypes.func,
  lastItem: React.PropTypes.bool,
  firstItem: React.PropTypes.bool
};

module.exports = OfferItem;
