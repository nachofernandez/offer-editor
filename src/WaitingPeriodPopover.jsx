import React from 'react';
import Popover from 'react-bootstrap/lib/Popover';
import Input from 'react-bootstrap/lib/Input';
import InlineEdit from 'react-edit-inline';
import Button from 'react-bootstrap/lib/Button';
import OverlayTrigger from 'react-bootstrap/lib/OverlayTrigger';

const DEFAULT_DAYS = '999';
const PERIODS = [
  {value: 'period1', label: 'First of the Month after Date of Hire'},
  {value: 'period2', label: 'First of the Month after _DAYS_ day(s) following Date of Hire'},
  {value: 'period3', label: 'Date of Hire'},
  {value: 'period4', label: '_DAYS_ day(s) following Date of Hire'}
];

class WaitingPeriodPopover extends React.Component{
  constructor(props) {
    super(props);

    if (this.props.entity.waitingPeriod) {
      this.state =  {selected: this.props.entity.waitingPeriod.selected, days: this.props.entity.waitingPeriod.days};
    } else {
      this.state =  {days: DEFAULT_DAYS};
    }
  }

  _onChangeHandler(e) {
    this.setState({selected: e.currentTarget.value});
  }

  dataChanged(data) {
    this.setState({days: data.days});
  }

  _removeSelected(e) {
    e.preventDefault();
    e.stopPropagation();

    let entity = this.props.entity;
    entity.waitingPeriod = null;
    this.props.onUpdate(entity);
    this.setState({selected: null, days: DEFAULT_DAYS});
  }

  _save() {
    let entity = this.props.entity;
    let period = _.findWhere(PERIODS, {value: this.state.selected});

    let periodLabel = period.label;
    if (periodLabel.indexOf('_DAYS_') > -1) {
      periodLabel = periodLabel.replace('_DAYS_', this.state.days);
    } else {
      this.setState({days:DEFAULT_DAYS});
    }

    entity.waitingPeriod = {label: periodLabel, days: this.state.days , selected: this.state.selected} ;

    this.props.onUpdate(entity);
    this.refs.popoverTrigger.hide();

    this.forceUpdate();
  }

  closePopover() {
    this.refs.popoverTrigger.hide();
  }

  _title() {
    return (
      <div>
        <span>+Add Default Waiting Period</span>
        <button type="button" className="close" onClick={this.closePopover.bind(this)}><span aria-hidden="true">&times;</span></button>
      </div>
    )
  }

  _popOver() {
    return (
      <Popover {...this.props} id="waitingPopover" className="waiting-popover" title={this._title()}>
        <form className="default-waiting-form">
          <div className="radio">
            <label>
              <input type="radio" checked={this.state.selected === 'period1'}
                     onChange={this._onChangeHandler.bind(this)} name="waitingPeriod" value="period1" />
              First of the Month after Date of Hire
            </label>
          </div>
          <div className="radio">
            <label>
              <input type="radio" checked={this.state.selected === 'period2'}
                     onChange={this._onChangeHandler.bind(this)} name="waitingPeriod" value="period2" />
              First of the Month after&nbsp;
              <InlineEdit className="editable-click" activeClassName="name-input-short" text={this.state.days} paramName="days" change={this.dataChanged.bind(this)}   />
              &nbsp;day(s) following Date of Hire
            </label>
          </div>
          <div className="radio">
            <label>
              <input type="radio" checked={this.state.selected === 'period3'}
                     onChange={this._onChangeHandler.bind(this)} name="waitingPeriod" value="period3" />
              Date of Hire
            </label>
          </div>
          <div className="radio">
            <label>
              <input type="radio" checked={this.state.selected === 'period4'}
                     onChange={this._onChangeHandler.bind(this)} name="waitingPeriod" value="period4" />
              <InlineEdit className="editable-click" activeClassName="name-input-short" text={this.state.days} paramName="days" change={this.dataChanged.bind(this)}   />
              &nbsp;day(s) following Date of Hire
            </label>
          </div>
          <button className="btn btn-success" type="button" onClick={this._save.bind(this)}>Save</button>
        </form>
      </Popover>
    )
  }

  _renderDefaultView() {
    return (
      <OverlayTrigger ref="popoverTrigger" trigger="click" placement="right" rootClose overlay={this._popOver()}>
        {this.props.entity.waitingPeriod ?
          <span className="waiting-label">Default Waiting Period&nbsp;
            <label className="selected-waiting-period">
              <span>{this.props.entity.waitingPeriod.label}</span>
              <span className="selection__choice__remove" onClick={this._removeSelected.bind(this)} role="presentation">×</span>
            </label>
          </span> :
          <Button bsStyle="link">+ Add default Waiting Period</Button>}
      </OverlayTrigger>
    )
  }

  _renderBenefitView() {
    return (
      <OverlayTrigger ref="popoverTrigger" trigger="click" placement="top" rootClose overlay={this._popOver()}>
        {this.props.entity.waitingPeriod ?
           <label className="selected-waiting-period">
              <span>{this.props.entity.waitingPeriod.label}</span>
              <span className="selection__choice__remove" onClick={this._removeSelected.bind(this)} role="presentation">×</span>
           </label>
          : <Button bsStyle="link">+ Add Benefit Waiting Period</Button>}
      </OverlayTrigger>
    )
  }

  getClass() {
    if (this.props.defaultView) {
      return 'waiting-period-container';
    } else if (this.props.entity.waitingPeriod){
      return 'waiting-period-container benefit-view'
    } else {
      return 'waiting-period-container benefit-view empty'
    }
  }

  render() {
    return (
      <div className={this.getClass()}>
        {this.props.defaultView ? this._renderDefaultView() : this._renderBenefitView()}
        {!this.props.defaultView && this.props.entity.waitingPeriod ? <span className="placeholder">Benefit Waiting Period</span> : null}
      </div>);
  }
}

/**
 * Entity: Object with waitingPeriod attribute
 * onUndate: Function called after set a new waitingPeriod to the passed entity
 * @type {{entity: object, onUpdate: function}}
 */
WaitingPeriodPopover.propTypes = {
  defaultView: React.PropTypes.bool,
  entity: React.PropTypes.object,
  onUpdate: React.PropTypes.func
};

module.exports = WaitingPeriodPopover;
