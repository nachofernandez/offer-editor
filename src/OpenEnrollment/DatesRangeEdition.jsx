import React from 'react';
import BigOverLay from '../../lib/BigOverLay';

import MonthSelect from './../utils/MonthSelect.jsx';
import YearSelect from './../utils/YearSelect.jsx';

import {monthYearToDateMoment} from '../utils/Dates.jsx';

class DatesRangeEdition extends React.Component{
  constructor(props) {
    super(props);
    this.state = {period: {}, from: {month: null, year: null}, to: {month: null, year: null}, errors: []};
  }

  componentWillMount() {
    this.syncState(this.props.period);
  }

  componentWillReceiveProps(nextProps) {
    this.syncState(nextProps.period);
  }

  syncState(period) {
    if (period) {
      let fromMoment = moment(period.from);
      let toMoment = moment(period.to);

      this.setState({
        period: _.clone(period), errors: [],
        from: {month: fromMoment.format('M'), year:fromMoment.format('YYYY')},
        to: {month: toMoment.format('M'), year:toMoment.format('YYYY')}
      });

    } else {
      this.setState({
        period: {}, errors: [],
        from: {month: null, year: null},
        to: {month: null, year: null}
      });
    }
  }

  fromDateChange(newValue) {
    let from = this.state.from;
    from.month = newValue;
    this.setState({from: from});
  }

  toDateChange(newValue) {
    let to = this.state.to;
    to.month = newValue;
    this.setState({to: to});
  }

  fromYearChange(newValue) {
    let from = this.state.from;
    from.year = newValue;
    this.setState({from: from});
  }

  toYearChange(newValue) {
    let to = this.state.to;
    to.year = newValue;
    this.setState({to: to});
  }


  validate() {
    let valid = true;
    let errors = [];

    if (!this.state.from.month) {
      valid = false;
      errors.push('fromMonth');
    }

    if (!this.state.from.year) {
      valid = false;
      errors.push('fromYear');
    }

    if (!this.state.to.month) {
      valid = false;
      errors.push('toMonth');
    }

    if (!this.state.to.year) {
      valid = false;
      errors.push('toYear');
    }

    this.setState({errors: errors});
    return valid;
  }

  _save() {
    if (this.validate()) {

      let dateFrom = monthYearToDateMoment(this.state.from.month, this.state.from.year);
      this.state.period.from = dateFrom ? dateFrom.format() : '';

      let dateTo = monthYearToDateMoment(this.state.to.month, this.state.to.year);
      let endDateTo = dateTo ? dateTo.endOf('month') : null;

      this.state.period.to = endDateTo ? endDateTo.format() : '';

      this.props.onUpdate(this.state.period);
      this.props.onHide();
    }
  }

  render() {
    return (
      <BigOverLay header="Open Enrollment Window" show={this.props.show} onHide={this.props.onHide}>
        <form className="row default-waiting-form dates-edition">
          <div className="col-md-12">
              <div className="col-md-6">
                <div className={_.contains(this.state.errors, 'fromMonth') ? 'form-group has-error' : 'form-group'}>
                  <label>Start date on first day of</label>
                  <MonthSelect onChange={this.fromDateChange.bind(this)} value={this.state.from.month} />
                </div>
                <div className={_.contains(this.state.errors, 'fromYear') ? 'has-error' : null}>
                  <YearSelect onChange={this.fromYearChange.bind(this)} value={this.state.from.year} />
                </div>
              </div>
              <div className="col-md-6">
                <div className={_.contains(this.state.errors, 'toMonth') ? 'form-group has-error' : 'form-group'}>
                  <label>Ends on last day of</label>
                  <MonthSelect onChange={this.toDateChange.bind(this)} value={this.state.to.month} />
                </div>
                <div className={_.contains(this.state.errors, 'toYear') ? 'has-error' : null}>
                  <YearSelect onChange={this.toYearChange.bind(this)} value={this.state.to.year} />
                </div>
              </div>
          </div>
          <div className="col-md-12">
            <button className="btn btn-success" type="button" onClick={this._save.bind(this)}>Save</button>
          </div>
        </form>
      </BigOverLay>
    );
  }
}

DatesRangeEdition.propTypes = {
  show: React.PropTypes.bool,
  onHide: React.PropTypes.func,
  period: React.PropTypes.object, //From and To dates
  onUpdate: React.PropTypes.func
};

module.exports = DatesRangeEdition;
