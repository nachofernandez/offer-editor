import React from 'react';
import ActionsBar from './ActionsBar.jsx';
import PeopleTable from './PeopleTable.jsx';

class EnrollmentTable extends React.Component{
  constructor(props) {
    super(props);
    this.state = {selectedItems: []}
  }

  componentWillMount() {
    this.setState({selectedItems: []});
  }

  actionForSelect(field, value) {
    _.forEach(this.state.selectedItems, (person) => {
      person[field] = value;
      this.props.onUpdatePerson(person);
    } );
  }

  selectAll() {
    let reset = (this.state.selectedItems.length === this.props.list.length);
    this.setState({selectedItems: reset ? [] : _.clone(this.props.list)});
  }

  toggleSelect(person) {
    let selectedItems = this.state.selectedItems;
    if (_.find(selectedItems, person)) {
      //Remove from selected
      this.setState({selectedItems: _.reject(selectedItems, person)});
    } else {
      selectedItems.push(person);
      this.setState({selectedItems: selectedItems});
    }
  }

  render() {
    return (
      <div>
        <div className="plan-editor container">
          <ActionsBar list={this.props.list} selectedItems={this.state.selectedItems}
                      actionForSelect={this.actionForSelect.bind(this)} onSelectAll={this.selectAll.bind(this)}/>
        </div>

        <div className="plan-editor container table-container">
          <PeopleTable list={this.props.list} onSearch={this.props.onSearch}
                       onUpdatePerson={this.props.onUpdatePerson} counts={this.props.counts}
                       selectedItems={this.state.selectedItems} onToggleSelect={this.toggleSelect.bind(this)} />
        </div>
      </div>
    );
  }
}

EnrollmentTable.propTypes = {
  list: React.PropTypes.array,
  onSearch: React.PropTypes.func,
  onUpdatePerson: React.PropTypes.func,
  counts: React.PropTypes.object
};

module.exports = EnrollmentTable;
