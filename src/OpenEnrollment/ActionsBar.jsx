import React from 'react';

class ActionsBar extends React.Component{
  constructor(props) {
    super(props);
    this.state = {selected: null};
  }

  actionsSelected(e) {
    let selected = e.target.value;

    switch (selected) {
      case 'Make Excluded':
        this.props.actionForSelect('excluded', true);
        break;
      case 'Remove from Excluded':
        this.props.actionForSelect('excluded', false);
        break;
    }

    this.setState({selected: selected});
  }

  render() {
    return (
      <div className="filters-bar">
        <div className="views pull-left">
          <select className="form-control">
            <option value="">Views</option>
            <option value="Self Service On">Self Service On</option>
            <option value="Self Service Pending">Self Service Pending</option>
            <option value="View 3">View 3</option>
            <option value="View 4">View 4</option>
          </select>
        </div>
        <div className="selection pull-left">
          <button className="btn btn-link" onClick={this.props.onSelectAll}>Select Page</button>
          <button className="btn btn-link" onClick={this.props.onSelectAll}>
            {this.props.selectedItems.length === this.props.list.length ? 'Unselect All' : 'Select All'}</button>
        </div>
        <div className="selected-label pull-left">
          <span>You have selected {this.props.selectedItems.length} / {this.props.list.length}</span>
        </div>
        <div className="actions pull-left">
          <select className="form-control" value={this.state.selected || ''} onChange={this.actionsSelected.bind(this)}>
            <option value="">Actions for selected</option>
            <option value="Make Excluded">Make Excluded</option>
            <option value="Remove from Excluded">Remove from Excluded</option>
          </select>
        </div>
      </div>
    );
  }
}

ActionsBar.propTypes = {
  list: React.PropTypes.array,
  selectedItems: React.PropTypes.array,
  onSelectAll: React.PropTypes.func,
  actionForSelect: React.PropTypes.func
};

module.exports = ActionsBar;
