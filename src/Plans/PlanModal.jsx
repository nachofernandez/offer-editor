import React from 'react';
import BigOverLay from '../../lib/BigOverLay';

import MonthSelect from './../utils/MonthSelect.jsx';
import YearSelect from './../utils/YearSelect.jsx';

import {monthYearToDateMoment} from '../utils/Dates.jsx';

class PlanModal extends React.Component{
  constructor(props) {
    super(props);
    this.state = {planPeriod: {}, from: {month: null, year: null}, to: {month: null, year: null}, errors: []};
  }

  componentWillMount() {
    this.syncState(this.props.planPeriod);
  }

  componentWillReceiveProps(nextProps) {
    this.syncState(nextProps.planPeriod);
  }

  syncState(planPeriod) {
    if (planPeriod) {
      let fromMoment = moment(planPeriod.from);
      let toMoment = moment(planPeriod.to);

      this.setState({
        planPeriod: _.clone(planPeriod), errors: [],
        from: {month: fromMoment.format('M'), year:fromMoment.format('YYYY')},
        to: {month: toMoment.format('M'), year:toMoment.format('YYYY')}
      });

    } else {
      this.setState({
        planPeriod: {}, errors: [],
        from: {month: null, year: null},
        to: {month: null, year: null}
      });
    }
  }

  nameChange(e) {
    let planPeriod = this.state.planPeriod;
    planPeriod.name = e.currentTarget.value;
    this.setState({planPeriod: planPeriod});
  }

  fromDateChange(newValue) {
    let from = this.state.from;
    from.month = newValue;
    this.setState({from: from});
  }

  toDateChange(newValue) {
    let to = this.state.to;
    to.month = newValue;
    this.setState({to: to});
  }

  fromYearChange(newValue) {
    let from = this.state.from;
    from.year = newValue;
    this.setState({from: from});
  }

  toYearChange(newValue) {
    let to = this.state.to;
    to.year = newValue;
    this.setState({to: to});
  }

  companyChange(e) {
    let planPeriod = this.state.planPeriod;
    planPeriod.company = e.currentTarget.value;
    this.setState({planPeriod: planPeriod});
  }

  validate() {
    let valid = true;
    let errors = [];

    if (!this.state.planPeriod.name) {
      valid = false;
      errors.push('name');
    }

    if (!this.state.from.month) {
      valid = false;
      errors.push('fromMonth');
    }

    if (!this.state.from.year) {
      valid = false;
      errors.push('fromYear');
    }

    if (!this.state.to.month) {
      valid = false;
      errors.push('toMonth');
    }

    if (!this.state.to.year) {
      valid = false;
      errors.push('toYear');
    }

    this.setState({errors: errors});
    return valid;
  }

  _save() {
    if (this.validate()) {

      let dateFrom = monthYearToDateMoment(this.state.from.month, this.state.from.year);
      this.state.planPeriod.from = dateFrom ? dateFrom.format() : '';

      let dateTo = monthYearToDateMoment(this.state.to.month, this.state.to.year);
      let endDateTo = dateTo ? dateTo.endOf('month') : null;

      this.state.planPeriod.to = endDateTo ? endDateTo.format() : '';

      this.props.updatePeriod(this.state.planPeriod);
      this.props.onHide();
    }
  }

  _employersOptions() {
    return this.props.employers.map(function(employer, index) {
      return (<option key={index} value={employer.name}>{employer.name}</option>);
    }, this);
  }

  render() {
    return (
      <BigOverLay header="Add Plan Period" show={this.props.show} onHide={this.props.onHide}>
        <form className="row default-waiting-form">
          <div className="col-md-12">
            <div className="col-md-4">
              <div className="col-md-12">&nbsp;</div>
              <div className={_.contains(this.state.errors, 'name') ? 'form-group has-error' : 'form-group'}>
                <label>Plan Period Name</label>
                <input type="text" className="form-control" value={this.state.planPeriod.name || ''} onChange={this.nameChange.bind(this)} placeholder="Plan Period Name" />
              </div>
              <div className="form-group">
                <label>Employer offering the benefits</label>
                <select className="form-control"
                        onChange={this.companyChange.bind(this)}
                        value={this.state.planPeriod.company ? this.state.planPeriod.company : '' } >
                  <option value="">Select...</option>
                  {this._employersOptions()}
                </select>
              </div>
            </div>
            <div className="col-md-8">
                <div className="col-md-12">
                  <label>Default Coverage</label>
                </div>
                <div className="col-md-6">
                  <div className={_.contains(this.state.errors, 'fromMonth') ? 'form-group has-error' : 'form-group'}>
                    <label>Start date on first day of</label>
                    <MonthSelect onChange={this.fromDateChange.bind(this)} value={this.state.from.month} />
                  </div>
                  <div className={_.contains(this.state.errors, 'fromYear') ? 'has-error' : null}>
                    <YearSelect onChange={this.fromYearChange.bind(this)} value={this.state.from.year} />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className={_.contains(this.state.errors, 'toMonth') ? 'form-group has-error' : 'form-group'}>
                    <label>Ends on last day of</label>
                    <MonthSelect onChange={this.toDateChange.bind(this)} value={this.state.to.month} />
                  </div>
                  <div className={_.contains(this.state.errors, 'toYear') ? 'has-error' : null}>
                    <YearSelect onChange={this.toYearChange.bind(this)} value={this.state.to.year} />
                  </div>
                </div>
            </div>
          </div>
          <div className="col-md-12"></div>
          <div className="col-md-12">
            <button className="btn btn-success" type="button" onClick={this._save.bind(this)}>Save</button>
          </div>
        </form>
      </BigOverLay>
    );
  }
}

PlanModal.propTypes = {
  show: React.PropTypes.bool,
  onHide: React.PropTypes.func,
  planPeriod: React.PropTypes.object,
  updatePeriod: React.PropTypes.func,
  employers: React.PropTypes.array
};

module.exports = PlanModal;
