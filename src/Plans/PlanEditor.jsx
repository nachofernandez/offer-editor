import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import PlanPeriod from './PlanPeriod.jsx';
import PlanFromLibraryModal from './PlanFromLibraryModal.jsx';
import AddPlanModal from './AddPlanModal.jsx';

const PLAN_DEFAULT_VALUE = {deductionType: 'Pre-Tax', groupStatus: 'existing'};
const DATE_FORMAT = 'D-MMM-YYYY';

class PlanEditor extends React.Component{
  constructor(props) {
    super(props);
    this.state = {showFromLibraryModal: false, showAddPlan: false, plan: PLAN_DEFAULT_VALUE};
  }

  _plans() {
    let plansList = this.props.plansList;
    return plansList.map(function(plan, index) {
      return (
      <li key={'itemkey'+index} className="col-md-12 plan-item" onClick={this.editPlan.bind(this, plan)}>
        <div className="img-container">
          <img src={plan.imageUrl ? plan.imageUrl : 'https://placeholdit.imgix.net/~text?txtsize=15&txt=Logo&w=100&h=50'} width="100" height="50" />
        </div>
        <div className="name-container">
          <span>{plan.name}</span><br />
          <span>
            {plan.vendor} &nbsp; {plan.deductionType} &nbsp;  {plan.groupNo ? 'Group #' + plan.groupNo : null}
          </span>
        </div>
        <div className="coverage-container">
          <span>{moment(plan.from).format(DATE_FORMAT)} / {moment(plan.to).format(DATE_FORMAT)}</span>
        </div>
        <div className="actions-container">
          <a className="btn btn-link dropdown-toggle not-open" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            <span className="not-open">Actions</span>
            <span className="glyphicon glyphicon-menu-down not-open" />
          </a>
        </div>
      </li>
      );
    }, this);
  }

  _onHideModal() {
    this.setState({showFromLibraryModal: false});
  }

  showModal() {
    this.setState({showFromLibraryModal: true});
  }

  _onHideAddPlan() {
    this.setState({showAddPlan: false});
  }

  createNewPlanModal(template) {
    let plan;

    if (template) {
      template.from = template.CanStartFrom;
      template.to = template.CanStartUntil;
      template.templateId = template.objectId;
      delete template.CanStartFrom;
      delete template.CanStartUntil;
      delete template.objectId;
      delete template.createdAt;
      delete template.updatedAt;
      plan = _.extend(PLAN_DEFAULT_VALUE, template);
    }
    this.setState({showAddPlan: true, showFromLibraryModal: false, plan: plan});
  }

  editPlan(plan, e) {
    if (e.nativeEvent.target.textContent != 'Actions') {
      this.setState({showAddPlan: true, showFromLibraryModal: false, plan: plan});
    }
  }

  savePlan(plan) {
    this.props.onSavePlan(plan);
  }

  render() {
    return (
      <div className="plan-editor container">
        <AddPlanModal show={this.state.showAddPlan} onHide={this._onHideAddPlan.bind(this)}
                      ratesOptions={this.props.ratesOptions}
                      onSave={this.savePlan.bind(this)} plan={this.state.plan}/>

        {this.props.hideList ? null :
          <PlanFromLibraryModal show={this.state.showFromLibraryModal} onHide={this._onHideModal.bind(this)}
                                templates={this.props.templates} onSearch={this.props.onSearchTemplate}
                                typesOptions={this.props.typesOptions} vendorsOptions={this.props.vendorsOptions}
                                onShowAddPlan={this.createNewPlanModal.bind(this)}/> }

        <PlanPeriod {...this.props} planPeriod={this.props.planPeriod} updatePeriod={this.props.updatePeriod}/>
        {this.props.hideList ? null :
          <div>
            <div className="process-header">
              <div className="col-md-5">
                <h2 className="appTitle">1 - Add Plans</h2>
              </div>
              <Button bsStyle="success" onClick={this.showModal.bind(this)} className="btn-green pull-right">+ Add Plan from Library</Button>
            </div>

            <div className="plans-list">
              <div className="plans-list-header">
                <span className="pull-left">Name</span>
                <span className="pull-right">Coverage Period</span>
              </div>
              <ul className="items-list">
                {this.props.plansList.length > 0 ? this._plans() : null}
              </ul>
            </div>
          </div>
          }
      </div>
    );
  }
}

PlanEditor.propTypes = {
  editPlan: React.PropTypes.func,
  planPeriod: React.PropTypes.object,
  updatePeriod: React.PropTypes.func,
  plansList: React.PropTypes.array,
  onSavePlan: React.PropTypes.func,
  employers: React.PropTypes.array,
  hideList: React.PropTypes.bool,
  typesOptions: React.PropTypes.array,
  vendorsOptions: React.PropTypes.array,
  ratesOptions: React.PropTypes.array,
  templates: React.PropTypes.array,
  onSearchTemplate: React.PropTypes.func
};

module.exports = PlanEditor;
