import React from 'react';
import ReactDOM from 'react-dom';
import Button from 'react-bootstrap/lib/Button';
import PlanModal from './PlanModal.jsx';

import {formatDatesPeriod} from '../utils/Dates.jsx';

class PlanEditor extends React.Component{
  constructor(props) {
    super(props);
    this.state = {modalOpen: false}
  }

  openModal() {
    this.setState({modalOpen: true});
  }

  hideModal() {
    this.setState({modalOpen: false});
  }

  statusChange(e) {
    let period = this.props.planPeriod;
    period.status = e.currentTarget.value;
    this.props.updatePeriod(period);
  }

  render() {
    let planPeriod = this.props.planPeriod;
    return (
      <div className="row plan-period">
        <PlanModal show={this.state.modalOpen}  onHide={this.hideModal.bind(this)}
                   employers={this.props.employers} planPeriod={this.props.planPeriod}
                   updatePeriod={this.props.updatePeriod} />

        <div className="col-md-6 plan-details" onClick={this.openModal.bind(this)}>
          <div className="plan-name">
            <h3>{planPeriod.name}</h3>
          </div>
          <div>
            <label>Default coverage period from:</label>&nbsp;
            <label className="editable-click">{formatDatesPeriod(planPeriod.from, planPeriod.to)}</label>
          </div>
          <div>
            <label>Plan Period is offered by</label>&nbsp;
            <label className="editable-click">{planPeriod.company}</label>
          </div>
        </div>
        <div className="col-md-3 period-status pull-right">
          <span>Period Status:</span><br />
          <select className="form-control" onChange={this.statusChange.bind(this)} value={planPeriod.status}>
            <option value="Preparing">Preparing</option>
            <option value="Open Enrollment in Progress">Open Enrollment in Progress</option>
            <option value="Accepting Individual Enrollment">Accepting Individual Enrollment</option>
            <option value="Expired">Expired</option>
            <option value="Terminated">Terminated</option>
          </select>
        </div>
      </div>
    );
  }
}

PlanEditor.propTypes = {
  planPeriod: React.PropTypes.object,
  updatePeriod: React.PropTypes.func,
  employers: React.PropTypes.array
};

module.exports = PlanEditor;
