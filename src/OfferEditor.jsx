import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import OfferList from './OfferList.jsx';
import RuleBuilder from './RuleBuilder.jsx';

class OfferEditor extends React.Component{
  constructor(props) {
    super(props);
    this.state = {openRuleBuilder: false, currentOffer: null};
  }

  onShowRuleBuilder(offer) {
    this.setState({openRuleBuilder: true, currentOffer: offer})
  }

  onHideRuleBuilder() {
    this.setState({openRuleBuilder: false, currentOffer: null});
  }

  saveRule(ruleString) {
    let offer = this.state.currentOffer;
    offer.rule = ruleString;
    this.props.onUpdateOffer(offer);
    this.onHideRuleBuilder();
  }

  render() {
    var { onAddOffer, offers, plansList, ...others } = this.props;
    return (
      <div className="offerEditor container">
        <RuleBuilder show={this.state.openRuleBuilder} onSave={this.saveRule.bind(this)} onHide={this.onHideRuleBuilder.bind(this)} />

        <div className="process-header">
          <div className="col-md-5">
            <h2 className="appTitle">2 - Configure Process</h2>
          </div>
          <Button bsStyle="success" className="btn-green pull-right" onClick={onAddOffer}>+ Add Offering</Button>
        </div>

        <div className="process-sub-title">
          <span className="pull-right">(The rules are matched in the order shown below)</span>
        </div>

        <div className="eligibility-list">
            {offers.length > 0 && plansList.length > 0 ? <OfferList {...others} onShowRuleBuilder={this.onShowRuleBuilder.bind(this)} offers={offers} plansList={plansList} /> : null}
        </div>
      </div>
    );
  }
}

OfferEditor.propTypes = {
  plansList: React.PropTypes.array,
  offers: React.PropTypes.array,
  onAddOffer: React.PropTypes.func,
  onUpdateOffer: React.PropTypes.func
};

module.exports = OfferEditor;
