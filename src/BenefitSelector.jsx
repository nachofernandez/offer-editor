import React from 'react';
import WaitingPeriodPopover from './WaitingPeriodPopover.jsx';
import ContributionToBenefitMultiple from './Contribution/ContributionToBenefitMultiple.jsx';
import ContributionToBenefitSingle from './Contribution/ContributionToBenefitSingle.jsx';

class BenefitSelector extends React.Component{
  constructor(props) {
    super(props);
  }

  componentDidMount() {
      let $planElement = $(this.refs[this.props.plan]);
      let $selectElement = $planElement.select2({
        placeholder: '',
        data: this.props.plansList
      });

      $selectElement.on('change', (event) => {
        this._toggleAddPlanText(event, $planElement);
      });

      if (this.props.benefit.plans) {
        $planElement.select2().val(this.props.benefit.plans).trigger('change');
      }

      $selectElement.on('change', (event, data) => {
        if (data) {  //highlight added element
          let $element = $(event.target.nextSibling).find('li[title=\''+data.text+'\']');
          $element.effect('highlight');
        }

        let benefitObject = this.props.benefit;
        benefitObject.plans = $planElement.val();
        this.props.onUpdateBenefit(benefitObject, this.props.plan);
      });
  }

  _toggleAddPlanText(event, $planElement) {
    var $addPlanElement = $(event.target.nextSibling).find('li.select2-search--inline');

    if ($planElement.val() &&
      $planElement.val().length === this.props.plansList.length) {
      //Hide add plan text
      $addPlanElement.hide();
    } else {
      $addPlanElement.show();
    }
  }

  _updateBenefitData(benefitObject) {
    this.props.onUpdateBenefit(benefitObject, this.props.planTypes[this.props.plan]);
    this.forceUpdate();
  }

  render() {
    let addOptionsToContribution = true;
    /*let planType = this.props.planTypes[this.props.plan];
    if (planType === this.props.planTypes.MEDICAL || planType === this.props.planTypes.DENTAL || planType === this.props.planTypes.VISION) {
      addOptionsToContribution = true;
    }*/

    return (
      <div className="benefit-selector">
        <div>
          <label className="capitalize item-label">{this.props.planTypes[this.props.plan]}</label>
          <div className="plans-selection pull-right">
            <select ref={this.props.plan} className="form-control" multiple="multiple" />
          </div>
        </div>

        <div className="text-right">
          <WaitingPeriodPopover entity={this.props.benefit} onUpdate={this._updateBenefitData.bind(this)}/>
          {addOptionsToContribution ?
            <ContributionToBenefitMultiple entity={this.props.benefit} disable={this.props.disableContribution} onUpdate={this._updateBenefitData.bind(this)}/> :
            <ContributionToBenefitSingle entity={this.props.benefit} disable={this.props.disableContribution} onUpdate={this._updateBenefitData.bind(this)}/>
          }
        </div>
      </div>
    );
  }
}

/**
 * plan: plan name
 * planTypes: Enum with available plans
 * plansList: List of options for the passed plan
 * onUpdateOffer: Called after update offer
 * Offer: offer Object
 * @type {{plan: String, offer: Object, planTypes: Object, plansList: Array, onUpdateOffer: Function}}
 */

BenefitSelector.propTypes = {
  disableContribution: React.PropTypes.bool,
  plan: React.PropTypes.string,
  planTypes: React.PropTypes.object,
  benefit: React.PropTypes.object,
  plansList: React.PropTypes.array,
  onUpdateBenefit: React.PropTypes.func
};

module.exports = BenefitSelector;
