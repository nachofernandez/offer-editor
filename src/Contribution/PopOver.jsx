import React from 'react';
import Popover from '../../node_modules/react-bootstrap/lib/Popover';
import PremiumForm from './PremiumForm.jsx';
import FixedForm from './FixedForm.jsx';

const TYPES = {
  PERCENT: 'percent',
  FIXED: 'fixed'
};

class CafeteriaSelector extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      contribution: null,
      amount: null,
      noMoreAmount: null,
      typeSelect: TYPES.PERCENT
    }
  }

  componentWillMount() {
    let contribution = this.props.contribution;
    if (contribution) {
      this.setState({contribution: contribution, amount: contribution.amount, noMoreAmount: contribution.noMoreAmount, typeSelect: contribution.type});
    }
  }

  componentWillReceiveProps(nextProps) {
    let contribution = nextProps.contribution;
    if (contribution) {
      this.setState({amount: contribution.amount, noMoreAmount: contribution.noMoreAmount, typeSelect: contribution.type});
    } else {
      //Defaults
      this.setState({contribution: null, amount: null, noMoreAmount: null, typeSelect: TYPES.PERCENT});
    }
  }

  dataChanged(data) {
    this.setState(data);
  }

  _save() {
    let newContribution = {amount: this.state.amount, type: this.state.typeSelect, noMoreAmount: this.state.noMoreAmount};

    this.setState({contribution: newContribution});
    this.props.onUpdate(newContribution);

    this.props.onClose();
  }

  _handleComboSelect(e) {
    this.setState({typeSelect: e.target.value});
  }

  _title() {
    return (
      <div>
        <span>{this.props.title}</span>
        <button type="button" className="close" onClick={this.props.onClose}><span aria-hidden="true">&times;</span></button>
      </div>
    )
  }

  render() {
    return (
      <Popover {...this.props} id="contributionPopover" className="contributionPopover" title={this._title()}>
        <form className="contribution-form">
          <div className="row">
            <div className="col-md-offset-2 col-md-4">
              <label>Contribute</label>
            </div>
            <div className="col-md-4">
              <select className="form-control" onChange={this._handleComboSelect.bind(this)} value={this.state.typeSelect}>
                <option value="percent">% of Premium</option>
                <option value="fixed">Fixed Amount</option>
              </select>
            </div>
          </div>
          {this.state.typeSelect === TYPES.PERCENT ?
            <PremiumForm amount={this.state.amount} noMoreAmount={this.state.noMoreAmount} onUpdate={this.dataChanged.bind(this)}/> :
            <FixedForm amount={this.state.amount} onUpdate={this.dataChanged.bind(this)} />}
          <button className="btn btn-success" type="button" onClick={this._save.bind(this)}>Save</button>
        </form>
      </Popover>
    );
  }
}


CafeteriaSelector.propTypes = {
  title: React.PropTypes.string,
  contribution: React.PropTypes.object,
  onClose: React.PropTypes.func,
  onUpdate: React.PropTypes.func
};

module.exports = CafeteriaSelector;
