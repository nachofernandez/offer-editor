import React from 'react';
import Button from '../../node_modules/react-bootstrap/lib/Button';
import OverlayTrigger from '../../node_modules/react-bootstrap/lib/OverlayTrigger';
import PopOver from './PopOver.jsx';

const TYPES = {
  PERCENT: 'percent',
  FIXED: 'fixed'
};

class CafeteriaSelector extends React.Component{
  constructor(props) {
    super(props);
    this.state = {contribution: null};
  }

  componentWillMount() {
      this.setState({contribution: this.props.entity.contribution});
  }

  componentWillReceiveProps(nextProps) {
    this.setState({contribution: nextProps.entity.contribution});
  }

  _buildLabel() {
    let label = '';
    let contribution = this.props.entity.contribution; //From props to prevent changes before save

    if (contribution.type === TYPES.FIXED) {
      label += 'fixed amount of $' + contribution.amount + ' per month';
    } else {
      label += contribution.amount + '% of the premium';

      if (contribution.noMoreAmount) {
          label += ' no more than $' + contribution.noMoreAmount;
      }
    }

    return label;
  }

  _removeSelected() {
    this.setState({contribution: null});
    this.onUpdateContribution(null);
  }

  onUpdateContribution(selectedValues) {
    let entity = this.props.entity;
    entity.contribution = selectedValues;
    this.props.onUpdate(entity);
  }

  closePopover() {
    this.refs.contributionPopoverTrigger.hide();
  }

  _cafeteriaButton() {
    if (this.state.contribution) {
      return (
        <span className="waiting-label">Cafeteria Plan&nbsp;
          <label className="selected-cafeteria">
            <span>{this._buildLabel()}</span>
            <span className="selection__choice__remove" onClick={this._removeSelected.bind(this)} role="presentation">×</span>
          </label>
        </span>);
    } else {
      return (<Button bsStyle="link">+ Add Cafeteria Plan</Button>);
    }
  }

  render() {
    return (
      <div className="waiting-period-container">
        {this.props.disable ?
          <button type="button" className="btn btn-link disabled">+ Add Cafeteria Plan</button>  :
          <OverlayTrigger ref="contributionPopoverTrigger" disabled trigger="click" placement="top" rootClose
                          overlay={<PopOver {...this.props} onClose={this.closePopover.bind(this)} title="Edit Cafeteria Plan"
                          contribution={this.state.contribution} onUpdate={this.onUpdateContribution.bind(this)}  />}>
            {this._cafeteriaButton()}
          </OverlayTrigger>}
      </div>
    );
  }
}

CafeteriaSelector.propTypes = {
  disable: React.PropTypes.bool,
  entity: React.PropTypes.object,
  onUpdate: React.PropTypes.func
};

module.exports = CafeteriaSelector;
