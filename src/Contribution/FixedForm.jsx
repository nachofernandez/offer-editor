import React from 'react';
import InlineEdit from 'react-edit-inline';

class FixedForm extends React.Component{
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="col-md-5">
        $ <InlineEdit className="editable-click" activeClassName="name-input-short" text={this.props.amount ? this.props.amount : '0' } paramName="amount" change={this.props.onUpdate} />
      </div>
    );
  }
}


FixedForm.propTypes = {
  amount: React.PropTypes.string,
  onUpdate: React.PropTypes.func
};

module.exports = FixedForm;
