import React from 'react';
import OfferItem from './OfferItem.jsx'

class OfferList extends React.Component{
  constructor(props) {
    super(props);
  }

  _offers() {
    var { offers, ...other } = this.props;
    return offers.map(function(offer, index) {
      return (
        <OfferItem {...other} key={offer.objectId} firstItem={index === 0} lastItem={offers.length-2 === index} eventKey={'e_'+offer.objectId} offer={offer} />
      );
    }, this);
  }

  render() {
    return (
      <ul className="items-list">
        {this._offers()}
      </ul>
    );
  }
}

OfferList.propTypes = {
  offers: React.PropTypes.array
};

module.exports = OfferList;
