import './offerEditorStyles.css';
import './react-select.css';

import React from 'react'
import { render } from 'react-dom'
import { hashHistory, Router, Route, Link, IndexRoute } from 'react-router'

import PlansPeriodList from './Plans/PlansPeriodList.jsx';
import PlanPeriodDetails from './Plans/PlanPeriodDetails.jsx';
import OpenEnrollmentPage from './OpenEnrollmentPage.jsx';

const App = React.createClass({
  contextTypes: {
    router: React.PropTypes.object.isRequired
  },

  render() {
    return (
      <div>
          {this.props.children}
      </div>
    )
  }
});



render((
  <Router history={hashHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={PlansPeriodList} />
      <Route path="details/:periodId" component={PlanPeriodDetails} />
      <Route path="dashboard/:periodId" component={OpenEnrollmentPage} />
    </Route>
  </Router>
), document.getElementById('container'));
