import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import Pagination from 'react-bootstrap/lib/Pagination';
import OfferStore from './stores/OfferStore';
import OffersActions from './actions/OffersActions';
import PlanEditor from './Plans/PlanEditor.jsx';
import EnrollmentTable from './OpenEnrollment/EnrollmentTable.jsx';
import DatesRangeEdition from './OpenEnrollment/DatesRangeEdition.jsx';

import {formatDatesPeriod} from './utils/Dates.jsx';

import { Link } from 'react-router';

const DEFAULT_PAGE_LIMIT = 10;

class OpenEnrollmentPage extends React.Component{
  constructor(props) {
    super(props);
    this.onStoreChange = this.onStoreChange.bind(this);
    this.state = _.extend({showEdition: false, activePage: 1, page_limit: DEFAULT_PAGE_LIMIT}, OfferStore.getState()); //Offers and Plans
  }

  onStoreChange(state) {
    if (!state.loadingPeriod && !state.loadingPeople) {
      this.unBlockUI();
    }
    this.setState(state);
  }

  componentDidMount() {
    this.blockUI();
    OfferStore.listen(this.onStoreChange);
    OffersActions.fetchPlansV2(this.props.params.periodId);
    OffersActions.fetchPlans();
    OffersActions.fetchEmployers();
    OffersActions.fetchVendors();
    OffersActions.fetchRates();
    OffersActions.getCounts(this.props.params.periodId);
    OffersActions.fetchPeople({limit: this.state.page_limit, offset: 0});
    OffersActions.fetchPeriod(this.props.params.periodId);
  }

  componentWillUnmount() {
    OfferStore.unlisten(this.onStoreChange);
  }

  add() {
    OffersActions.addOffer(this.state.offers, {name: null, default: false, order: 1, periodId: this.props.params.periodId });
  }

  remove(offer) {
    OffersActions.removeOffer(this.state.offers, offer);
  }

  updateOffer(offer) {
    OffersActions.updateOffer(offer);
  }

  sort(offer, newOrder) {
    OffersActions.syncOrder(this.state.offers, offer, newOrder);
  }

  updatePeriod(period) {
    OffersActions.changePeriod(period);
  }

  addPlanV2(plan) {
    plan.periodId = this.state.planPeriod.objectId;
    OffersActions.addPlanV2(plan);
  }

  updatePlanV2(plan) {
    OffersActions.editPlanV2(plan);
  }

  callUpload() {
    OffersActions.callUpload(this.state.planPeriod.objectId);
  }

  changePageLimit(e) {
    this.setState({page_limit: e.target.value}, this.fetchPeople);
  }

  searchPeople(filter) {
    //Reset page
    this.setState({activePage: 1, filter: filter}, this.fetchPeople);
  }

  updatePerson(person) {
    OffersActions.changePerson(person);
  }

  fetchPeople() {
    OffersActions.fetchPeople(_.extend({limit: this.state.page_limit, offset: (this.state.activePage-1)*this.state.page_limit}, this.state.filter), this.state.planPeriod.objectId);
  }

  savePlanV3(plan) {
    plan.objectId ? this.updatePlanV2(plan) : this.addPlanV2(plan);
  }

  _selectPage(event, data) {
    let pageNum = data.eventKey;
    this.setState({activePage: pageNum}, this.fetchPeople);
  }

  updateEnrollmentPeriod(newPeriod) {
    let planPeriod = this.state.planPeriod;

    planPeriod.enrollmentFrom = newPeriod.from;
    planPeriod.enrollmentTo = newPeriod.to;

    this.updatePeriod(planPeriod);
  }

  showDatesEdition() {
    this.setState({showEdition: true});
  }

  onHideEdition() {
    this.setState({showEdition: false});
  }

  blockUI() {
    $(this.refs.mainDiv).block({overlayCSS: {opacity: 0.3}, message: null });
  }

  unBlockUI() {
    $(this.refs.mainDiv).unblock();
  }


  render() {
    let planPeriod = this.state.planPeriod;
    return (
      <div ref="mainDiv">
          <DatesRangeEdition period={{from: planPeriod.enrollmentFrom, to: planPeriod.enrollmentTo}} onHide={this.onHideEdition.bind(this)}
                             show={this.state.showEdition} onUpdate={this.updateEnrollmentPeriod.bind(this)} />

          <div className="plan-editor container">
            <div className="row plan-period dashboard-sub-header">
              <div className="col-md-6">
                <Link to={`/details/${planPeriod.objectId}`}>
                  <button className="btn-green btn btn-success">Edit Plan Period Configuration</button>
                </Link>
              </div>
              <div className="col-md-6 text-right">
                  <label>Open Enrollment Window From:</label>&nbsp;
                  <label className="editable-click" onClick={this.showDatesEdition.bind(this)}>{formatDatesPeriod(planPeriod.enrollmentFrom, planPeriod.enrollmentTo)}</label>
              </div>
            </div>
          </div>
          <PlanEditor planPeriod={planPeriod} plansList={this.state.plansV2} hideList
                      vendorsOptions={this.state.vendors} ratesOptions={this.state.rates} employers={this.state.employers}
                      onSavePlan={this.savePlanV3.bind(this)} updatePeriod={this.updatePeriod.bind(this)} />
          <div className="plan-editor container">
            <div className="row plan-period">
              <button className="btn btn-link" onClick={this.callUpload.bind(this)}>Upload</button>
            </div>
          </div>

          <EnrollmentTable list={this.state.people.results || []} counts={this.state.counts}
                           onSearch={this.searchPeople.bind(this)} onUpdatePerson={this.updatePerson.bind(this)}/>
          <div className="plan-editor container">
            <div className="row plan-period">
              <span>Show <input className="page-limit" type="number" value={this.state.page_limit} onChange={this.changePageLimit.bind(this)}/> people per page</span>
              <Pagination className="pull-right"
                prev
                next
                first
                last
                ellipsis
                boundaryLinks
                items={Math.ceil(this.state.people.count/this.state.page_limit)}
                maxButtons={3}
                activePage={this.state.activePage}
                onSelect={this._selectPage.bind(this)} />
            </div>
          </div>
      </div>
    );
  }
}

OpenEnrollmentPage.propTypes = {
  params: React.PropTypes.object
};

module.exports = OpenEnrollmentPage;
